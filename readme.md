# Pindahan ke situs saya

Proyek ini telah dipindahkan ke proyek [dpqb-web](https://gitlab.com/dpqb-web/dpqb-web) karena suatu masalah teknis dalam GitLab.

[Klik disini untuk melihat situs saya.](https://dpqb-web.gitlab.io/dpqb-web/)
