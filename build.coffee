path = require "path"
fs = require "fs"
pug = require "pug"

publik = path.resolve "public"
dir_pug = path.resolve "pug"

situs = "https://dpqb-web.gitlab.io/dpqb-web/"

fs.readdirSync(dir_pug)
.map (file) =>
    path.join dir_pug, file
.forEach (file) =>
    nama = path.basename file, ".pug"
    nama_html = nama + ".html"
    
    isi = pug.renderFile file, situs: situs
    fs.writeFileSync path.join(publik, nama_html), isi
